﻿using DemoMongoDbConnectionTable.DTOs;
using DemoMongoDbConnectionTable.Models;

namespace DemoMongoDbConnectionTable.Services.Pro
{
    public interface IProductService
    {
        public Task<List<Product>> GetAsync();
        public Task<List<ProductVm>> GetAll();
        public Task<ProductVm> GetByIdAsync(string Id);
        public Task CreateAsync(Product Product);
        public Task UpdateAsync(string Id, Product Product);
        public Task DeleteAsync(string Id);
    }
}
