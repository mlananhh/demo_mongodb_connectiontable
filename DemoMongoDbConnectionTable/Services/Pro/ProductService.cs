﻿using DemoMongoDbConnectionTable.DTOs;
using DemoMongoDbConnectionTable.Helper;
using DemoMongoDbConnectionTable.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace DemoMongoDbConnectionTable.Services.Pro
{
    public class ProductService : IProductService
    {
        private readonly IMongoCollection<Product> _productCollecttion;
        private readonly IMongoCollection<Category> _categoryCollection;
        public ProductService(IOptions<MongoDbSetting> mongoDbSetting)
        {
            var mongoClient = new MongoClient(
                mongoDbSetting.Value.ConnectionURL);

            var mongoDatabase = mongoClient.GetDatabase(
                mongoDbSetting.Value.DatabaseName);

            _productCollecttion = mongoDatabase.GetCollection<Product>("Products");
            _categoryCollection = mongoDatabase.GetCollection<Category>("Categories");
        }

        public async Task<List<Product>> GetAsync() => await _productCollecttion.Find(_ => true).ToListAsync();
        public async Task<List<Category>> GetCategoryAsync() => await _categoryCollection.Find(_ => true).ToListAsync();
        //using LINQ
     /*   public async Task<ProductVm> GetByIdAsync(string Id) {
            var products = await GetAsync();
            var categories = await GetCategoryAsync();

            
            var query = from product in products
                        join category in categories
                        on product.CategoryId equals category.Id
                        where product.Id == Id
                        select new ProductVm
                        {
                            Id = product.Id,
                            ProductName = product.ProductName,
                            CategoryId = product.CategoryId,
                            CategoryName = category.CategoryName
                        };

            return query.FirstOrDefault();
        }*/
     //using lambda
        public async Task<ProductVm> GetByIdAsync(string id)
        {
            var products = await GetAsync();
            var categories = await GetCategoryAsync();

            var result = products
                .Where(product => product.Id == id)
                .Join(
                    categories,
                    product => product.CategoryId,
                    category => category.Id,
                    (product, category) => new ProductVm
                    {
                        Id = product.Id,
                        ProductName = product.ProductName,
                        CategoryId = product.CategoryId,
                        CategoryName = category.CategoryName
                    })
                .FirstOrDefault();

            return result;
        }


        public async Task CreateAsync(Product Product) => await _productCollecttion.InsertOneAsync(Product);
        public async Task UpdateAsync(string Id, Product Product) => await _productCollecttion.ReplaceOneAsync(x => x.Id == Id, Product);
        public async Task DeleteAsync(string Id) => await _productCollecttion.DeleteOneAsync(x => x.Id == Id);

        public async Task<List<ProductVm>> GetAll()
        {
            var products = await GetAsync();
            var categories = await GetCategoryAsync();

            var query = from product in products
                        join category in categories
                        on product.CategoryId equals category.Id
                        select new ProductVm
                        {
                            Id = product.Id,
                            ProductName = product.ProductName,
                            CategoryId = product.CategoryId,
                            CategoryName = category.CategoryName
                        };

            return query.ToList();
        }
    }
}
