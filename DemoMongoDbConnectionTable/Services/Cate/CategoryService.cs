﻿using DemoMongoDbConnectionTable.Helper;
using DemoMongoDbConnectionTable.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace DemoMongoDbConnectionTable.Services.Cate
{
    public class CategoryService : ICategoryService
    {
        private readonly IMongoCollection<Category> _CategoryCollecttion;
        public CategoryService(IOptions<MongoDbSetting> mongoDbSetting)
        {
            var mongoClient = new MongoClient(
                mongoDbSetting.Value.ConnectionURL);

            var mongoDatabase = mongoClient.GetDatabase(
                mongoDbSetting.Value.DatabaseName);

            _CategoryCollecttion = mongoDatabase.GetCollection<Category>("Categories");
        }

        public async Task<List<Category>> GetCatAsync() => await _CategoryCollecttion.Find(_ => true).ToListAsync();
        public async Task<Category> GetByIdAsync(string Id) => await _CategoryCollecttion.Find(x => x.Id == Id).FirstOrDefaultAsync();
        public async Task CreateAsync(Category Category) => await _CategoryCollecttion.InsertOneAsync(Category);
        public async Task UpdateAsync(string Id, Category Category) => await _CategoryCollecttion.ReplaceOneAsync(x => x.Id == Id, Category);
        public async Task DeleteAsync(string Id) => _CategoryCollecttion.DeleteOneAsync(x => x.Id == Id);
    }
}
