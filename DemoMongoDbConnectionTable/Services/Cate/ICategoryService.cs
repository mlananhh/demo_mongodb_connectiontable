﻿using DemoMongoDbConnectionTable.Models;

namespace DemoMongoDbConnectionTable.Services.Cate
{
    public interface ICategoryService
    {
        public Task<List<Category>> GetCatAsync();
        public Task<Category> GetByIdAsync(string Id);
        public Task CreateAsync(Category Category);
        public Task UpdateAsync(string Id, Category Category);
        public Task DeleteAsync(string Id);
    }
}
