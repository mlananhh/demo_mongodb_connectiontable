﻿using DemoMongoDbConnectionTable.DTOs;
using DemoMongoDbConnectionTable.Models;
using DemoMongoDbConnectionTable.Services.Pro;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoMongoDbConnectionTable.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        public ProductsController(IProductService productService)
        {
           _productService = productService;
        }
        [HttpGet]
        public async Task<List<ProductVm>> GetAll() => await _productService.GetAll();

        [HttpGet("GetById/{Id}")]
        public async Task<IActionResult> GetById(string Id)
        {
            var item = await _productService.GetByIdAsync(Id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create(Product item)
        {
            await _productService.CreateAsync(item);
            return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
        }

        [HttpPut("Update/{Id}")]
        public async Task<IActionResult> Update(string Id, Product item)
        {
            var itemUpdate = await _productService.GetByIdAsync(Id);
            if (itemUpdate is null)
            {
                return NotFound();
            }
            item.Id = itemUpdate.Id;
            await _productService.UpdateAsync(Id, item);
            return Ok();
        }

        [HttpDelete("Delete/{Id}")]
        public async Task<IActionResult> Delete(string Id)
        {
            var itemUpdate = await _productService.GetByIdAsync(Id);
            if (itemUpdate is null)
            {
                return NotFound();
            }

            await _productService.DeleteAsync(Id);
            return Ok();
        }
    }
}
