﻿using DemoMongoDbConnectionTable.Models;
using DemoMongoDbConnectionTable.Services.Cate;
using Microsoft.AspNetCore.Mvc;

namespace DemoMongoDbConnectionTable.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryService _cateService;
        public CategoriesController(ICategoryService categoryService)
        {
            _cateService = categoryService;
        }
        [HttpGet]
        public async Task<List<Category>> GetAll() => await _cateService.GetCatAsync();

        [HttpGet("GetById/{Id}")]
        public async Task<IActionResult> GetById(string Id)
        {
            var item = await _cateService.GetByIdAsync(Id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create(Category item)
        {
            await _cateService.CreateAsync(item);
            return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
        }

        [HttpPut("Update/{Id}")]
        public async Task<IActionResult> Update(string Id, Category item)
        {
            var itemUpdate = await _cateService.GetByIdAsync(Id);
            if (itemUpdate is null)
            {
                return NotFound();
            }
            item.Id = itemUpdate.Id;
            await _cateService.UpdateAsync(Id, item);
            return Ok();
        }

        [HttpDelete("Delete/{Id}")]
        public async Task<IActionResult> Delete(string Id)
        {
            var itemUpdate = await _cateService.GetByIdAsync(Id);
            if (itemUpdate is null)
            {
                return NotFound();
            }

            await _cateService.DeleteAsync(Id);
            return Ok();
        }
    }
}
