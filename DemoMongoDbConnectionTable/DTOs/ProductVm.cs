﻿namespace DemoMongoDbConnectionTable.DTOs
{
    public class ProductVm
    {
        public string Id { get; set; }
        public string ProductName { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
